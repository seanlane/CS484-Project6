/*
 *
 * File:            mandelbrot.cpp
 * Author:          Dany Shaanan
 * Website:         http://danyshaanan.com
 * File location:   https://github.com/danyshaanan/mandelbrot/blob/master/cpp/mandelbrot.cpp
 *
 * Created somewhen between 1999 and 2002
 * Rewritten August 2013
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <algorithm>
#include <stdint.h>
#include <sys/time.h>
#include <cstring>

////////////////////////////////////////////////////////////////////////////////

float iterationsToEscape(double x, double y, int maxIterations);
void master();
void receive_results(unsigned char * img, MPI_Status * status);
void slave(int rank);
double When();

////////////////////////////////////////////////////////////////////////////////

const int MAX_WIDTH_HEIGHT = 30000;
const int HUE_PER_ITERATION = 5;
const bool DRAW_ON_KEY = true;

const int SLAVE_REQUEST = 0;                   // Tag for when slave is requesting work
const int SLAVE_RESULTS = 1;                   // Tag for when slave is sending results
const int MASTER_KILL   = 2;                   // Tag for when there is no more work :)

const int CHUNK_MIN     = 100;          
const float CHUNK_REDUCE= 0.5; 
const bool DEBUG        = false;
const int CHUNK_SIZE    = 14000;

////////////////////////////////////////////////////////////////////////////////

class State {
    public:
        double centerX;
        double centerY;
        double zoom;
        int maxIterations;
        int w;
        int h;
        State() {
            //centerX = -.75;
            //centerY = 0;
            centerX = -1.186340599860225;
            centerY = -0.303652988644423;
            zoom = 1;
            maxIterations = 100;
            w = 28000;
            h = 28000;
        }
};

////////////////////////////////////////////////////////////////////////////////

float iterationsToEscape(double x, double y, int maxIterations) {
    double tempa;
    double a = 0;
    double b = 0;
    for (int i = 0 ; i < maxIterations ; i++) {
        tempa = a*a - b*b + x;
        b = 2*a*b + y;
        a = tempa;
        if (a*a+b*b > 64) {
            // return i; // discrete
            return i - log(sqrt(a*a+b*b))/log(8); //continuous
        }
    }
    return -1;
}

int hue2rgb(float t){
    while (t>360) {
        t -= 360;
    }
    if (t < 60) return 255.*t/60.;
    if (t < 180) return 255;
    if (t < 240) return 255. * (4. - t/60.);
    return 0;
}

void writeImage(unsigned char *img, int w, int h) {
    long long filesize = 54 + 3*(long long)w*(long long)h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    FILE *f;
    f = fopen("temp.bmp","wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for (int i=0; i<h; i++) {
        long long offset = ((long long)w*(h-i-1)*3);
        fwrite(img+offset,3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }
    fclose(f);
}

////////////////////////////////////////////////////////////////////////////////

void master()
{
    fprintf(stdout, "Master Start; Chunk Size: %d; Reduction: %f; Min Chunk: %d\n",
        CHUNK_SIZE, CHUNK_REDUCE, CHUNK_MIN);
    double start = When();
    MPI_Status status;

    State state;
    int w = state.w;
    int h = state.h;

    if (w > MAX_WIDTH_HEIGHT) w = MAX_WIDTH_HEIGHT;
    if (h > MAX_WIDTH_HEIGHT) h = MAX_WIDTH_HEIGHT;

    unsigned char r, g, b;
    unsigned char *img = NULL;
    if (img) free(img);
    long long size = (long long)w*(long long)h*3;
    if(DEBUG) printf("Malloc w %zu, h %zu,  %zu\n",w,h,size);
    img = (unsigned char *)malloc(size);
    if(DEBUG) printf("malloc returned %X\n",img);

    bool keep_working = true;
    int px = 0, py = 0;
    int chunk_size = CHUNK_SIZE;
    int dto [4];
    while(keep_working)
    {
        // Get a work request from any slave node. Puny slaves.
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if(DEBUG) 
        {
            fprintf(stderr, "Master received request type #%d from Slave #%d\n", 
                status.MPI_TAG, status.MPI_SOURCE);
        }
    
        // If we are distributing work
        if(status.MPI_TAG == SLAVE_REQUEST)
        {
            // Receive the flag message to discard it
            MPI_Recv(NULL, 0, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, 
                MPI_COMM_WORLD, &status);

            // Grab a chunk of work and distribute to the node
            dto[0] = px; dto[1] = py;
            dto[2] = std::min(px + chunk_size, w);
            dto[3] = std::min(py + chunk_size, h); 
            MPI_Send(dto, 4, MPI_INT, status.MPI_SOURCE, SLAVE_REQUEST, MPI_COMM_WORLD);
            
            // Move the indexes forward
            if ((px + chunk_size) >= w)
            {
                // If we reached the side edge of the grid, go to the left and move down
                px = 0;
                if((py + chunk_size) >= h)
                {
                    // If we have reached the end of the grid, call it    
                    keep_working = false;
                }
                else
                {
                    // If we haven't reached the edge, move down and then adjusting chunk size
                    // We only reduce chunk size on row shift to make sure we don't miss any
                    py += chunk_size;
                    chunk_size = std::max(CHUNK_MIN, (int)floor(chunk_size * CHUNK_REDUCE));
                }
            }
            else
            {
                // If we haven't reached the side, then all we need to do is move along the row
                px += chunk_size;
            }
        }
        else if(status.MPI_TAG == SLAVE_RESULTS)
        {
            receive_results(img, &status);
        }
        else // If we get here, I have no idea what happened
        {
            printf("Puny slave tried to communincate with the master inappropriately!\n");
            printf("Message tag: %d. Punish the slave, great master!\n", status.MPI_TAG);             
            MPI_Abort(MPI_COMM_WORLD, 0);
        }
    }
    
    if(DEBUG) fprintf(stdout, "Master ran out of work at %f seconds\n", When());
    
    // When there is no more work, keep receiving results and kill slaves
    int nproc, count = 0;    
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    while(count < nproc - 1)
    {
        // Start getting remaining requests
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        // If the slave wants works...
        if(status.MPI_TAG == SLAVE_REQUEST)
        {
            // Receive the flag message to discard it
            MPI_Recv(NULL, 0, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, 
                MPI_COMM_WORLD, &status);

            // Tell the slave that we are out of soup; NO SOUP FOR YOU!!! KILL SLAVE!!!
            MPI_Send(NULL, 0, MPI_INT, status.MPI_SOURCE, MASTER_KILL, MPI_COMM_WORLD); 
            if(DEBUG) 
            {
                fprintf(stdout, "Master is killing slave #%d, count is %d/%d\n",
                    status.MPI_SOURCE, count, nproc - 1); 
            }
            // One down, nproc - count to go...
            count++;
        }
        else if(status.MPI_TAG == SLAVE_RESULTS)
        {
            receive_results(img, &status);
        }
        else // If we get here, I have no idea what happened
        {
            printf("Puny slave tried to communincate with the master inappropriately!\n");
            printf("Message tag: %d. Punish the slave, great master!\n", status.MPI_TAG);
            MPI_Abort(MPI_COMM_WORLD, 0);
        }
    }
    
    fprintf(stdout, "Finished receiving, total time: %f\n", When() - start);
    writeImage(img, state.w, state.h);
    free(img);
}

////////////////////////////////////////////////////////////////////////////////

void receive_results(unsigned char * img, MPI_Status * status)
{
    if(DEBUG) fprintf(stderr, "Master receiving from Slave #%d\n", status->MPI_SOURCE);
    int dto [4];
    
    // Get the dimensions from the slave about what we are receiving
    MPI_Recv(dto, 4, MPI_INT, status->MPI_SOURCE, SLAVE_RESULTS, MPI_COMM_WORLD, status);
    if(DEBUG) 
    {
        fprintf(stderr, "Master received following dimensions: 0: %d; 1: %d; 2: %d; 3: %d\n",
            dto[0], dto[1], dto[2], dto[3]);
    }

    int chunk_px = dto[0]; int chunk_py = dto[1];
    int chunk_w = dto[2] - dto[0];
    int chunk_h = dto[3] - dto[1];
    long long message_size = (long long)chunk_h * (long long)chunk_w * 3;

    unsigned char * chunk_img = (unsigned char *)malloc(message_size);

    if(DEBUG)
    {
        fprintf(stderr, "c_px: %d, c_py: %d, c_w: %d, c_h: %d, m_s: %lld\n",
            chunk_px, chunk_py, chunk_w, chunk_h, message_size);
    }

    
    if (DEBUG) fprintf(stderr, "From slave #%d receive: %lld bytes\n", status->MPI_SOURCE, message_size);
    MPI_Recv((void *)chunk_img, message_size, MPI_UNSIGNED_CHAR, status->MPI_SOURCE, 
            SLAVE_RESULTS, MPI_COMM_WORLD, status);
    
    // Copy each row of the chunk to the appropriate spot within the image array
    for(int i = 0; i < chunk_h; i++)
    {
        // Find the location for this row within img
        long long loc = ((long long)chunk_px + ((long long)(chunk_py + i)) * (long long)chunk_w) * 3;
        memcpy((void *)&img[loc], (void *)&chunk_img[i * (long long)chunk_w * 3], (long long)chunk_w * 3); 
    }
}

////////////////////////////////////////////////////////////////////////////////

void slave(int rank)
{
    MPI_Status status;

    State state;
    int w = state.w;
    int h = state.h;

    if (w > MAX_WIDTH_HEIGHT) w = MAX_WIDTH_HEIGHT;
    if (h > MAX_WIDTH_HEIGHT) h = MAX_WIDTH_HEIGHT;   
 
    double xs[MAX_WIDTH_HEIGHT], ys[MAX_WIDTH_HEIGHT];
    for (int px=0; px<w; px++) 
    {
        xs[px] = (px - w/2)/state.zoom + state.centerX;
    }

    for (int py=0; py<h; py++) 
    {
        ys[py] = (py - h/2)/state.zoom + state.centerY;
    }

    bool keep_working = true;
    int dto[4];
    while(keep_working)
    {
        // Tell the master that we are ready, receive work task
        MPI_Send(NULL, 0, MPI_INT, 0, SLAVE_REQUEST, MPI_COMM_WORLD);
        MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if(status.MPI_TAG == SLAVE_REQUEST)
        {
                MPI_Recv(dto, 4, MPI_INT, 0, SLAVE_REQUEST, MPI_COMM_WORLD, &status);

                // Sanity check on the dimensions
                if( dto[0] >= w || dto[1] >= h || dto[2] > w || dto[3] > h ||
                    dto[0] > dto[2] || dto[1] > dto[3])
                {
                    fprintf(stderr, "Slave #%d messed up! Dimensions are erroneous!\n", rank);
                    fprintf(stderr, "DTO vals; 0: %d; 1: %d; 2: %d; 3: %d\n");
                    MPI_Abort(MPI_COMM_WORLD, 0);
                }

                // Using dimensions, malloc space for buffer and get to work
                int chunk_w = dto[2] - dto[0];
                int chunk_h = dto[3] - dto[1]; 

                unsigned char r, g, b;
                unsigned char *img = NULL;
                long long size = (long long)chunk_w * (long long)chunk_h * 3;
                    
                if(DEBUG) 
                {   
                    fprintf(stderr, "Slave #%d, Malloc w %zu, h %zu,  %zu\n", rank, chunk_w, chunk_h, size);
                }
                img = (unsigned char*)malloc(size);
                if(DEBUG) fprintf(stderr, "Slave #%d, Malloc returned %X\n", rank, img);

                for (int px = dto[0]; px < dto[2]; px++) 
                {   
                    for (int py = dto[1]; py < dto[3]; py++) 
                    {
                        r = g = b = 0;
                        float iterations = iterationsToEscape(xs[px], ys[py], state.maxIterations);
                        if (iterations != -1) 
                        {
                            float h = HUE_PER_ITERATION * iterations;
                            r = hue2rgb(h + 120);
                            g = hue2rgb(h);
                            b = hue2rgb(h + 240);
                        }
                    
                        long long loc = ((long long)(px - dto[0])+(long long)(py - dto[1])*(long long)chunk_w)*3;
                        img[loc+2] = (unsigned char)(r);
                        img[loc+1] = (unsigned char)(g);
                        img[loc+0] = (unsigned char)(b);
                    }
                }
                
                // Send the result back to the master
                if(DEBUG) 
                {
                    fprintf(stderr, "Slave #%d, begin sending results\n", rank);
                    fprintf(stderr, "dto: 0: %d; 1: %d; 2: %d; 3: %d; m_s: %d\n",
                        dto[0], dto[1], dto[2], dto[3], size); 
                }
                MPI_Send(dto, 4, MPI_INT, 0, SLAVE_RESULTS, MPI_COMM_WORLD);                
                MPI_Send((void *)img, size, MPI_UNSIGNED_CHAR, 0, SLAVE_RESULTS, MPI_COMM_WORLD);   
            
                // Free the malloc'd memory
                free(img);
                // Mind blowing, I know
        }
        else if(status.MPI_TAG == MASTER_KILL)
        {
            keep_working = false;
        }
        else
        {
            printf("Forgive me master, slave #%d know not how to handle MPI_TAG: %d!\n", 
                rank, status.MPI_TAG);
            MPI_Abort(MPI_COMM_WORLD, 0);
        }
    }    
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char ** argv) {

    // Initialize MPI
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
        master();
    else
        slave(rank);
   
    MPI_Finalize();
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

/* Return the correct time in seconds, using a double precision number.       */
double When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
