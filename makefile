MPI_HOME        = /users/faculty/snell/mpich
MPI_INCL        = $(MPI_HOME)/include
MPI_LIB         = $(MPI_HOME)/lib

SRC   			= mand.cpp
TARGET     		= mand

CC         		= mpic++
CFLAGS			= -O -I$(MPI_INCL)
LFLAGS     		= -lm

$(TARGET): $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(TARGET) $(LFLAGS)


run: $(TARGET)
	sbatch --nodes=32 run.sh

clean:
		rm -f  *.out

do:
	make clean
	make $(TARGET)
	make run

